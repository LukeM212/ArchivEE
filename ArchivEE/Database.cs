﻿using EELVL;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Drawing;
using System.IO;

namespace ArchivEE
{
	public class ArchivEEDB : DbContext
	{
		public DbSet<Player> Player { get; private set; } = null!;
		public DbSet<Staff> Staff { get; private set; } = null!;
		public DbSet<Crew> Crew { get; private set; } = null!;
		public DbSet<World> World { get; private set; } = null!;
		public DbSet<Campaign> Campaign { get; private set; } = null!;
		public DbSet<CampaignWorld> CampaignWorld { get; private set; } = null!;
		public DbSet<CompletedCampaignWorld> CompletedCampaignWorld { get; private set; } = null!;

		private struct Configuration
		{
			public string dbFile;
			public bool debug;
		}

		private Configuration? config = null;

		public ArchivEEDB(string dbFile, bool debug = false)
		{
			config = new Configuration {
				dbFile = dbFile,
				debug = debug,
			};

			using SqliteConnection connection = new(new SqliteConnectionStringBuilder {
				DataSource = dbFile,
				Mode = SqliteOpenMode.ReadWrite,
			}.ToString());

			connection.Open();

			using DbCommand command = connection.CreateCommand();

			command.CommandText = @"
				CREATE VIEW IF NOT EXISTS player_friend_transitive_closure(player_1, player_2) AS
				WITH RECURSIVE rec AS (
						SELECT player_1, player_2
						FROM player_friend
					UNION
						SELECT f1.player_1, f2.player_2
						FROM rec AS f1
						INNER JOIN player_friend AS f2 ON f1.player_2 = f2.player_1
				)
				SELECT * FROM rec
			";
			command.ExecuteNonQuery();

			command.CommandText = @"
				CREATE VIEW IF NOT EXISTS world_portal_transitive_closure(world, target) AS
				WITH RECURSIVE rec AS (
						SELECT world, target
						FROM world_portal
					UNION
						SELECT p1.world, p2.target
						FROM rec AS p1
						INNER JOIN world_portal AS p2 ON p1.target = p2.world
				)
				SELECT * FROM rec
			";
			command.ExecuteNonQuery();
		}

		protected override void OnConfiguring(DbContextOptionsBuilder options)
		{
			if (config is Configuration c)
			{
				options
					.UseLazyLoadingProxies()
					.UseSqlite(new SqliteConnectionStringBuilder {
						DataSource = c.dbFile,
						Mode = SqliteOpenMode.ReadOnly,
					}.ToString());
				if (c.debug) options
					.LogTo(Console.WriteLine, LogLevel.Information);
			}
		}

		protected override void OnModelCreating(ModelBuilder model)
		{
			model.Entity<Player>()
				.HasMany(p => p.Friends)
				.WithMany(p => p.Friended)
				.UsingEntity<PlayerFriend>(
					j => j
						.HasOne(f => f.Player2)
						.WithMany()
						.HasForeignKey(f => f.Player2Id),
					j => j
						.HasOne(f => f.Player1)
						.WithMany()
						.HasForeignKey(f => f.Player1Id));
			model.Entity<Player>()
				.HasMany(p => p.FriendsTransitiveClosure)
				.WithMany(p => p.FriendedTransitiveClosure)
				.UsingEntity<PlayerFriendTransitiveClosure>(
					j => j
						.HasOne(f => f.Player2)
						.WithMany()
						.HasForeignKey(f => f.Player2Id),
					j => j
						.HasOne(f => f.Player1)
						.WithMany()
						.HasForeignKey(f => f.Player1Id));
			model.Entity<Player>()
				.HasMany(c => c.Likes)
				.WithMany(w => w.Likes)
				.UsingEntity<PlayerLike>(
					j => j
						.HasOne(p => p.World)
						.WithMany()
						.HasForeignKey(p => p.WorldId),
					j => j
						.HasOne(p => p.Player)
						.WithMany()
						.HasForeignKey(p => p.PlayerId));
			model.Entity<Player>()
				.HasMany(c => c.Favorites)
				.WithMany(w => w.Favorites)
				.UsingEntity<PlayerFavorite>(
					j => j
						.HasOne(p => p.World)
						.WithMany()
						.HasForeignKey(p => p.WorldId),
					j => j
						.HasOne(p => p.Player)
						.WithMany()
						.HasForeignKey(p => p.PlayerId));

			model.Entity<Staff>()
				.HasOne(s => s.Player)
				.WithMany(p => p.StaffRoles)
				.HasForeignKey(s => s.PlayerId);

			model.Entity<Crew>()
				.HasOne(c => c.Owner)
				.WithMany(p => p.OwnedCrews)
				.HasForeignKey(c => c.OwnerId);
			model.Entity<Crew>()
				.HasMany(c => c.Members)
				.WithMany(p => p.Crews)
				.UsingEntity<CrewMember>(
					j => j
						.HasOne(c => c.Player)
						.WithMany()
						.HasForeignKey(c => c.PlayerId),
					j => j
						.HasOne(c => c.Crew)
						.WithMany()
						.HasForeignKey(c => c.CrewId));
			model.Entity<Crew>()
				.HasMany(c => c.Subscribed)
				.WithMany(p => p.Subscriptions)
				.UsingEntity<CrewSubscribed>(
					j => j
						.HasOne(c => c.Player)
						.WithMany()
						.HasForeignKey(c => c.PlayerId),
					j => j
						.HasOne(c => c.Crew)
						.WithMany()
						.HasForeignKey(c => c.CrewId));

			model.Entity<World>()
				.Property(w => w.BackgroundColor)
				.HasConversion(
					c => c != null ? (uint?)c.Value.ToArgb() : null,
					c => c != null ? (Color?)Color.FromArgb((int)c.Value) : null
				);
			model.Entity<World>()
				.HasOne(w => w.Owner)
				.WithMany(p => p.Worlds)
				.HasForeignKey(w => w.OwnerId);
			model.Entity<World>()
				.HasOne(w => w.Crew)
				.WithMany(c => c!.Worlds)
				.HasForeignKey(w => w.CrewId);
			model.Entity<World>()
				.HasOne(w => w.Data)
				.WithOne()
				.HasForeignKey<World>(w => w.RowId);
			model.Entity<World>()
				.HasMany(w => w.WorldPortals)
				.WithMany(w => w.OtherWorldPortals)
				.UsingEntity<WorldPortal>(
					j => j
						.HasOne(w => w.Target)
						.WithMany()
						.HasForeignKey(w => w.TargetId),
					j => j
						.HasOne(w => w.World)
						.WithMany()
						.HasForeignKey(w => w.WorldId));
			model.Entity<World>()
				.HasMany(w => w.WorldPortalsTransitiveClosure)
				.WithMany(w => w.OtherWorldPortalsTransitiveClosure)
				.UsingEntity<WorldPortalTransitiveClosure>(
					j => j
						.HasOne(w => w.Target)
						.WithMany()
						.HasForeignKey(w => w.TargetId),
					j => j
						.HasOne(w => w.World)
						.WithMany()
						.HasForeignKey(w => w.WorldId));

			model.Entity<CampaignWorld>()
				.HasOne(c => c.Campaign)
				.WithMany(c => c.CampaignWorlds)
				.HasForeignKey(c => c.CampaignId);
			model.Entity<CampaignWorld>()
				.HasOne(c => c.World)
				.WithOne(w => w.CampaignWorld!)
				.HasForeignKey<CampaignWorld>(c => c.WorldId);

			model.Entity<CompletedCampaignWorld>()
				.HasKey(c => new { c.PlayerId, c.CampaignWorldId });
			model.Entity<CompletedCampaignWorld>()
				.HasOne(c => c.Player)
				.WithMany(p => p.CompletedCampaignWorlds)
				.HasForeignKey(c => c.PlayerId);
			model.Entity<CompletedCampaignWorld>()
				.HasOne(c => c.CampaignWorld)
				.WithMany(c => c.Completed)
				.HasForeignKey(c => c.CampaignWorldId);
		}
	}

	[Table("player")]
	public record Player
	{
		[Key]
		[Column("rowid")]
		public ulong RowId { get; private set; }

		[Column("id")]
		public string Id { get; private set; } = null!;

		[Column("name")]
		public string Name { get; private set; } = null!;

		[Column("energy")]
		public uint Energy { get; private set; }

		[Column("created")]
		public DateTime? Created { get; private set; }

		[Column("last_login")]
		public DateTime? LastLogin { get; private set; }

		public virtual IReadOnlyCollection<Player> Friends { get; private set; } = null!;

		public virtual IReadOnlyCollection<Player> Friended { get; private set; } = null!;

		public virtual IReadOnlyCollection<Player> FriendsTransitiveClosure { get; private set; } = null!;

		public virtual IReadOnlyCollection<Player> FriendedTransitiveClosure { get; private set; } = null!;

		public virtual IReadOnlyCollection<Staff> StaffRoles { get; private set; } = null!;

		public virtual IReadOnlyCollection<Crew> OwnedCrews { get; private set; } = null!;

		public virtual IReadOnlyCollection<Crew> Crews { get; private set; } = null!;

		public virtual IReadOnlyCollection<Crew> Subscriptions { get; private set; } = null!;

		public virtual IReadOnlyCollection<World> Worlds { get; private set; } = null!;

		public virtual IReadOnlyCollection<CompletedCampaignWorld> CompletedCampaignWorlds { get; private set; } = null!;

		public virtual IReadOnlyCollection<World> Likes { get; private set; } = null!;

		public virtual IReadOnlyCollection<World> Favorites { get; private set; } = null!;
	}

	[Table("player_friend")]
	public record PlayerFriend
	{
		[Column("player_1")]
		public ulong Player1Id { get; private set; }
		public virtual Player Player1 { get; private set; } = null!;

		[Column("player_2")]
		public ulong Player2Id { get; private set; }
		public virtual Player Player2 { get; private set; } = null!;
	}

	[Table("player_friend_transitive_closure")]
	public record PlayerFriendTransitiveClosure
	{
		[Column("player_1")]
		public ulong Player1Id { get; private set; }
		public virtual Player Player1 { get; private set; } = null!;

		[Column("player_2")]
		public ulong Player2Id { get; private set; }
		public virtual Player Player2 { get; private set; } = null!;
	}

	[Table("staff")]
	public record Staff
	{
		[Key]
		[Column("rowid")]
		public ulong RowId { get; private set; }

		[Column("player")]
		public ulong PlayerId { get; private set; }
		public virtual Player Player { get; private set; } = null!;

		[Column("domain")]
		public string Domain { get; private set; } = null!;

		[Column("role")]
		public string Role { get; private set; } = null!;

		[Column("start")]
		public DateTime Start { get; private set; }

		[Column("end")]
		public DateTime? End { get; private set; }
	}

	[Table("crew")]
	public record Crew
	{
		[Key]
		[Column("rowid")]
		public ulong RowId { get; private set; }

		[Column("id")]
		public string Id { get; private set; } = null!;

		[Column("name")]
		public string Name { get; private set; } = null!;

		[Column("owner")]
		public ulong OwnerId { get; private set; }
		public virtual Player Owner { get; private set; } = null!;

		public virtual IReadOnlyCollection<Player> Members { get; private set; } = null!;

		public virtual IReadOnlyCollection<Player> Subscribed { get; private set; } = null!;

		public virtual IReadOnlyCollection<World> Worlds { get; private set; } = null!;
	}

	[Table("crew_member")]
	public record CrewMember
	{
		[Column("crew")]
		public ulong CrewId { get; private set; }
		public virtual Crew Crew { get; private set; } = null!;

		[Column("player")]
		public ulong PlayerId { get; private set; }
		public virtual Player Player { get; private set; } = null!;
	}

	[Table("crew_subscribed")]
	public record CrewSubscribed
	{
		[Column("player")]
		public ulong PlayerId { get; private set; }
		public virtual Player Player { get; private set; } = null!;

		[Column("crew")]
		public ulong CrewId { get; private set; }
		public virtual Crew Crew { get; private set; } = null!;
	}

	[Table("world")]
	public record World
	{
		[Key]
		[Column("rowid")]
		public ulong RowId { get; private set; }

		[Column("id")]
		public string Id { get; private set; } = null!;

		[Column("name")]
		public string Name { get; private set; } = null!;

		[Column("owner")]
		public ulong OwnerId { get; private set; }
		public virtual Player Owner { get; private set; } = null!;

		[Column("created")]
		public DateTime? Created { get; private set; }

		[Column("plays")]
		public uint Plays { get; private set; }

		[Column("crew")]
		public ulong? CrewId { get; private set; }
		public virtual Crew? Crew { get; private set; }

		[Column("crew_status")]
		public CrewWorldStatus? CrewStatus { get; private set; }

		[Column("description")]
		public string Description { get; private set; } = null!;

		[Column("width")]
		public uint Width { get; private set; }

		[Column("height")]
		public uint Height { get; private set; }

		[Column("background_color")]
		public Color? BackgroundColor { get; private set; }

		[Column("border_type")]
		public uint BorderType { get; private set; }

		[Column("gravity")]
		public float Gravity { get; private set; }

		[Column("minimap")]
		public bool Minimap { get; private set; }

		public virtual WorldData Data { get; private set; } = null!;

		private static SevenZip.Compression.LZMA.Decoder decoder;

		static World()
		{
			decoder = new();
			decoder.SetDecoderProperties(new byte[] { 93, 0, 0, 16, 0 });
		}

		public Level EELVL {
			get
			{
				Level level = new Level(
					Owner.Name,
					Name,
					(int)Width,
					(int)Height,
					Gravity,
					(uint?)BackgroundColor?.ToArgb() ?? 0,
					Description,
					CampaignWorld != null,
					Crew?.Id ?? "",
					Crew?.Name ?? "",
					(int?)CrewStatus ?? 0,
					Minimap,
					Owner.Id
				);

				MemoryStream compressed = new(Data.Data), decompressed = new();
				decoder.Code(compressed, decompressed, compressed.Length, -1, null);
				decompressed.Position = 0;
				level.ReadContent(decompressed);

				return level;
			}
		}

		public virtual IReadOnlyCollection<Player> Likes { get; private set; } = null!;

		public virtual IReadOnlyCollection<Player> Favorites { get; private set; } = null!;

		public virtual IReadOnlyCollection<World> WorldPortals { get; private set; } = null!;

		public virtual IReadOnlyCollection<World> OtherWorldPortals { get; private set; } = null!;

		public virtual IReadOnlyCollection<World> WorldPortalsTransitiveClosure { get; private set; } = null!;

		public virtual IReadOnlyCollection<World> OtherWorldPortalsTransitiveClosure { get; private set; } = null!;

		public virtual CampaignWorld? CampaignWorld { get; private set; }
	}

	[Table("world")]
	public record WorldData
	{
		[Key]
		[Column("rowid")]
		public ulong RowId { get; private set; }

		[Column("data")]
		public byte[] Data { get; private set; } = null!;
	}

	[Table("world_portal")]
	public record WorldPortal
	{
		[Column("world")]
		public ulong WorldId { get; private set; }
		public virtual World World { get; private set; } = null!;

		[Column("target")]
		public ulong TargetId { get; private set; }
		public virtual World Target { get; private set; } = null!;
	}

	[Table("world_portal_transitive_closure")]
	public record WorldPortalTransitiveClosure
	{
		[Column("world")]
		public ulong WorldId { get; private set; }
		public virtual World World { get; private set; } = null!;

		[Column("target")]
		public ulong TargetId { get; private set; }
		public virtual World Target { get; private set; } = null!;
	}

	[Table("campaign")]
	public record Campaign
	{
		[Key]
		[Column("rowid")]
		public ulong RowId { get; private set; }

		[Column("id")]
		public string Id { get; private set; } = null!;

		[Column("name")]
		public string Name { get; private set; } = null!;

		[Column("description")]
		public string Description { get; private set; } = null!;

		[Column("difficulty")]
		public CampaignDifficulty Difficulty { get; private set; }

		public virtual IReadOnlyCollection<CampaignWorld> CampaignWorlds { get; private set; } = null!;
	}

	[Table("campaign_world")]
	public record CampaignWorld
	{
		[Key]
		[Column("rowid")]
		public ulong RowId { get; private set; }

		[Column("campaign")]
		public ulong CampaignId { get; private set; }
		public virtual Campaign Campaign { get; private set; } = null!;

		[Column("stage")]
		public uint Stage { get; private set; }

		[Column("world")]
		public ulong WorldId { get; private set; }
		public virtual World World { get; private set; } = null!;

		[Column("difficulty")]
		public CampaignDifficulty Difficulty { get; private set; }

		[Column("bronze_time")]
		public uint? BronzeTime { get; private set; }

		[Column("silver_time")]
		public uint? SilverTime { get; private set; }

		[Column("gold_time")]
		public uint? GoldTime { get; private set; }

		[Column("platinum_time")]
		public uint? PlatinumTime { get; private set; }

		[Column("staff_time")]
		public uint? StaffTime { get; private set; }

		public virtual IReadOnlyCollection<CompletedCampaignWorld> Completed { get; private set; } = null!;
	}

	[Table("completed_campaign_world")]
	public record CompletedCampaignWorld
	{
		[Column("player")]
		public ulong PlayerId { get; private set; }
		public virtual Player Player { get; private set; } = null!;

		[Column("campaign_world")]
		public ulong CampaignWorldId { get; private set; }
		public virtual CampaignWorld CampaignWorld { get; private set; } = null!;

		[Column("time")]
		public uint? Time { get; private set; }
	}

	[Table("player_like")]
	public record PlayerLike
	{
		[Column("player")]
		public ulong PlayerId { get; private set; }
		public virtual Player Player { get; private set; } = null!;

		[Column("world")]
		public ulong WorldId { get; private set; }
		public virtual World World { get; private set; } = null!;
	}

	[Table("player_favorite")]
	public record PlayerFavorite
	{
		[Column("player")]
		public ulong PlayerId { get; private set; }
		public virtual Player Player { get; private set; } = null!;

		[Column("world")]
		public ulong WorldId { get; private set; }
		public virtual World World { get; private set; } = null!;
	}

	public enum CrewWorldStatus : sbyte
	{
		WIP = 0,
		Open = 1,
		Released = 2,
	}

	public enum CampaignDifficulty : sbyte
	{
		Basic = 0,
		BasicEasy = 1,
		Easy = 2,
		EasyMedium = 3,
		Medium = 4,
		EasyHard = 5,
		MediumHard = 6,
		Hard = 7,
		HardExtreme = 8,
		Extreme = 9,
		Insane = 10,
	}

	public enum CampaignRank : sbyte
	{
		Unranked = 0,
		Bronze = 1,
		Silver = 2,
		Gold = 3,
		Platinum = 4,
		Staff = 5,
	}
}
