﻿using ArchivEE;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

static class Helpers
{
	public static string Prompt(string message)
	{
		Console.Write($"{message}: ");
		return Console.ReadLine()!;
	}

	public static string PromptDefault(string message, string def)
	{
		string result = Prompt($"{message} ({def})");
		return string.IsNullOrWhiteSpace(result) ? def : result;
	}

	public static bool PromptBool(string message)
	{
		string result = Prompt($"{message} (Y/n)").ToLower();

		while (!new[] { "y", "n" }.Contains(result))
		{
			result = Prompt($"Please enter Y/n").ToLower();
		}

		return result == "y";
	}

	public static int PromptInt(string message)
	{
		string result = Prompt($"{message}").ToLower();

		int value;

		while (!int.TryParse(result, out value))
		{
			result = Prompt($"Please enter a valid integer").ToLower();
		}

		return value;
	}

	public static int? PromptIntMaybe(string message)
	{
		string result = Prompt($"{message} (integer or 'none')").ToLower();

		int value = 0;

		while (result != "none" && !int.TryParse(result, out value))
		{
			result = Prompt($"Please enter a valid integer").ToLower();
		}

		return result == "none" ? null : value;
	}

	public static uint PromptUInt(string message)
	{
		string result = Prompt($"{message}").ToLower();

		uint value;

		while (!uint.TryParse(result, out value))
		{
			result = Prompt($"Please enter a valid integer").ToLower();
		}

		return value;
	}

	public static uint? PromptUIntMaybe(string message)
	{
		string result = Prompt($"{message} (integer or 'none')").ToLower();

		uint value = 0;

		while (result != "none" && !uint.TryParse(result, out value))
		{
			result = Prompt($"Please enter a valid integer").ToLower();
		}

		return result == "none" ? null : value;
	}

	public static DateTime PromptDate(string message)
	{
		string result = Prompt($"{message} ('yyyy/mm/dd [hh:mm:ss]')").ToLower();

		DateTime date;

		while (!DateTime.TryParse(result, out date))
		{
			result = Prompt($"Please enter a date in the format 'yyyy/mm/dd [hh:mm:ss]'").ToLower();
		}

		return date;
	}

	public static DateTime? PromptDateMaybe(string message)
	{
		string result = Prompt($"{message} ('yyyy/mm/dd [hh:mm:ss]' or 'none')").ToLower();

		DateTime date = new();

		while (result != "none" && !DateTime.TryParse(result, out date))
		{
			result = Prompt($"Please enter a date in the format 'yyyy/mm/dd [hh:mm:ss]' or 'none'").ToLower();
		}

		return result == "none" ? null : date;
	}

	public static CampaignDifficulty? PromptDifficultyMaybe(string prompt)
	{
		return SelectMenu(prompt, new (string name, CampaignDifficulty? difficulty)[] {
			("Basic", CampaignDifficulty.Basic),
			("Basic-Easy", CampaignDifficulty.BasicEasy),
			("Easy", CampaignDifficulty.Easy),
			("Easy-Medium", CampaignDifficulty.EasyMedium),
			("Medium", CampaignDifficulty.Medium),
			("Easy-Hard", CampaignDifficulty.EasyHard),
			("Medium-Hard", CampaignDifficulty.MediumHard),
			("Hard", CampaignDifficulty.Hard),
			("Hard-Extreme", CampaignDifficulty.HardExtreme),
			("Extreme", CampaignDifficulty.Extreme),
			("Insane", CampaignDifficulty.Insane),
			("None", null),
		});
	}

	public static CampaignRank PromptCampaignRank(string prompt)
	{
		return SelectMenu(prompt, new (string name, CampaignRank difficulty)[] {
			("Unranked", CampaignRank.Unranked),
			("Bronze", CampaignRank.Bronze),
			("Silver", CampaignRank.Silver),
			("Gold", CampaignRank.Gold),
			("Platinum", CampaignRank.Platinum),
			("Staff", CampaignRank.Staff),
		});
	}

	public static CampaignRank GetRank(CompletedCampaignWorld c)
	{
		return new (uint? time, CampaignRank rank)[] {
			(c.CampaignWorld.StaffTime, CampaignRank.Staff),
			(c.CampaignWorld.PlatinumTime, CampaignRank.Platinum),
			(c.CampaignWorld.GoldTime, CampaignRank.Gold),
			(c.CampaignWorld.SilverTime, CampaignRank.Silver),
			(c.CampaignWorld.BronzeTime, CampaignRank.Bronze),
		}.FirstOrDefault(r => c.Time <= r.time).rank;
	}

	// Needed so that GetRank can be called within an SQL query
	public static Expression<Func<CompletedCampaignWorld, sbyte>> GetRankExpression = c =>
		c.Time <= c.CampaignWorld.StaffTime ? (sbyte)CampaignRank.Staff :
		c.Time <= c.CampaignWorld.PlatinumTime ? (sbyte)CampaignRank.Platinum :
		c.Time <= c.CampaignWorld.GoldTime ? (sbyte)CampaignRank.Gold :
		c.Time <= c.CampaignWorld.SilverTime ? (sbyte)CampaignRank.Silver :
		c.Time <= c.CampaignWorld.BronzeTime ? (sbyte)CampaignRank.Bronze :
		(sbyte)CampaignRank.Unranked;

	public static string? ReadNonEmpty()
	{
		string message = Console.ReadLine()!;
		return string.IsNullOrWhiteSpace(message) ? null : message;
	}

	public static string[] PromptList(string message)
	{
		Console.WriteLine("Enter one per line, then press enter twice when done");
		Console.WriteLine($"{message}:");

		List<string> results = new();
		while (ReadNonEmpty() is string result)
			results.Add(result);

		return results.ToArray();
	}

	public static string PromptMultiline(string message)
	{
		Console.WriteLine("Enter your multi-line answer, then press enter twice when done");
		Console.WriteLine($"{message}:");

		List<string> results = new();
		while (ReadNonEmpty() is string result)
			results.Add(result);

		return string.Join('\n', results);
	}

	public static void WriteColor(ConsoleColor color, string text)
	{
		ConsoleColor oldColor = Console.ForegroundColor;
		Console.ForegroundColor = color;
		Console.Write(text);
		Console.ForegroundColor = oldColor;
	}

	public static string MakeFilenameValid(string filename)
	{
		char[] invalids = Path.GetInvalidFileNameChars();
		invalids = invalids.Concat(new[] { '.' }).ToArray(); // EEO has a bug where it doesn't recognise eelvl files with dots in their names
		return string.Join("_", filename.Split(invalids));
	}

	public static T SelectMenu<T>(string prompt, IList<(string label, T value)> options)
	{
		Console.WriteLine(prompt);

		Console.WriteLine();

		for (int i = 0; i < options.Count; i++)
			Console.WriteLine($"{i + 1}. {options[i].label}");

		Console.WriteLine();

		string option = Prompt($"Selection (1-{options.Count})");
		int opt;
		while (!int.TryParse(option, out opt) || opt < 1 || opt > options.Count)
			option = Prompt($"Please enter a number between 1 and {options.Count}");

		Console.WriteLine();

		return options[opt - 1].value;
	}

	public static void ActionMenu(string prompt, IList<(string msg, Action action)> options)
	{
		SelectMenu(prompt, options)();
	}

	public static T FuncMenu<T>(string prompt, IList<(string msg, Func<T> action)> options)
	{
		return SelectMenu(prompt, options)();
	}
}
